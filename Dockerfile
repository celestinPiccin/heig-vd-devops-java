# Stage 1: Build stage using Maven
FROM maven:3.9.6-eclipse-temurin-17-alpine AS build
WORKDIR /opt/app

# Copy application source code into the container
COPY src ./src

# Copy Maven project file into the container
COPY pom.xml .

# Build the Maven project
RUN mvn package


# Stage 2: Runtime stage using Java
FROM eclipse-temurin:17-jre-alpine
WORKDIR /opt/app

# Copy compiled JAR file from the build stage
COPY --from=build opt/app/target/*.jar japp.jar
EXPOSE 8080

# Set the command to run the application
CMD ["java", "-jar", "/opt/app/japp.jar"]
